// #region Imports

/* Express */
import express, {Router} from 'express';

/* Hoard */
import giantbombRoutes from './giantbomb.routes';

// #endregion Imports

// #region Router

let router : Router = express.Router();
router.use(giantbombRoutes);

export default router;

// #endregion Router
