// #region Imports

/* Express  */
import express, {Application} from 'express';

/* DotEnv */
import dotenv from 'dotenv';
dotenv.config();

/* Cors */
import cors from 'cors';

/* Hoard */
import router from './router';

// #endregion Imports

// #region Constants

const API_PORT : number = 3000;

// #endregion Constants

// #region Application

let application : Application = express();

application.use(cors())
application.use(router);

application.listen(
  API_PORT,
  () => console.log('API server listening on port', API_PORT)
);

// #endregion Application