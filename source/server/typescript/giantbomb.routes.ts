// #region Imports

/* Express */
import express, {Router, Request, Response} from 'express';

/* Axios */
import axios, {AxiosResponse} from 'axios';

// #endregion Imports

// #region Constants

const API_KEY : string    = process.env.GIANTBOMB_API_KEY;
const API_BASE : string   = 'https://www.giantbomb.com/api/';

const API_ROUTE : string  = '/api/games/';

// #endregion Constants

// #region Router

let router : Router = express.Router();

/**
 * Basically, we want any route to api/giantbomb/ to serve as a passthrough.
 */
router.use(
  API_ROUTE + 'search/',
  (request : Request, response : Response) => {
    let query : any = request.query;
    query.api_key = API_KEY;

    axios.get(
      API_BASE + 'search/',
      {params: query}
    )
      .then((queryResponse : AxiosResponse) => response.status(queryResponse.status).send(queryResponse.data))
      .catch((reason : any) => response.send(reason));
  }
);

export default router;

// #endregion Router
