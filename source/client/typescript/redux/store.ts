// #region Imports

/* Redux */
import {createStore} from 'redux';

/* Hoard */
import reducers from './reducers';

// #endregion Imports

const store : any = createStore(reducers);
export default store;

