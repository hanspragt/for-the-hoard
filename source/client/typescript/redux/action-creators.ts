// #region Imports

/* Hoard */
import {ADD_GAME} from './action-types';

// #endregion Imports

// #region Actions

export interface Action {
  Command : string;
  Payload : any;
}

// #endregion Actions

// #region Action Generators

export function AddGame(name : string) : Action {
  return {
    Command: ADD_GAME,
    Payload: {
      Name: name
    }
  };
};

// #endregion Action Generators