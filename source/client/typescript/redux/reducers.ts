// #region Imports

/* Redux */
import {combineReducers} from 'redux';

/* Hoard */
import {Action} from './action-creators';
import {ADD_GAME} from './action-types';

// #endregion Imports

const reducers : any = combineReducers({GamesReducer});
export default reducers;

function GamesReducer(state : Array<string> = [], action : Action) : Array<string> {
  switch (action.Command) {
    case ADD_GAME: {
      return [...state, action.Payload.Name];
    }

    default: {
      return state;
    }
  }
}