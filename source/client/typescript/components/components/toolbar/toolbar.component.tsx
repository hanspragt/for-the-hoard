// #region Imports

/* React */
import * as React from 'react';

// #endregion Imports

// #region Component

export const Toolbar = () =>
(
  <div className='toolbar'>
    <h1>Toolbar</h1>
  </div>
);

// #endregion Component