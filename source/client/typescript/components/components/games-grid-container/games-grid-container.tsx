// #region Imports

/* Redux */
import {connect} from 'react-redux';

/* Hoard */
import {GamesGrid} from '../games-grid/games-grid.component';

// #endregion Imports

// #region Redux

const mapStateToProps = (state : any) => {
  return {
    Games : []
  };
};

const mapDispatchToProps = (dispatch : any) => {
  return {

  };
};

// #endregion Redux

// #region Component

export const GamesGridContainer = connect(mapStateToProps, mapDispatchToProps)(GamesGrid);

// #endregion Component