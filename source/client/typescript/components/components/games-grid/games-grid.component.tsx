// #region Imports

/* React */
import * as React from 'react';

// #endregion Imports

// #region Props

export interface GamesGridProps {
  Games : Array<string>;
}

// #endregion Props

// #region Component

export const GamesGrid = (props : GamesGridProps) =>
(
  <h1>Games Grid</h1>
);


// #endregion Component