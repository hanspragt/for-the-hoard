// #region Imports

/* React */
import * as React from 'react';

/* Hoard */
import {GamesGridContainer} from './components/games-grid-container/games-grid-container';
import {Toolbar} from './components/toolbar/toolbar.component';
import './application.styles.scss';

// #endregion Imports

// #region Component

export const Application = () =>
(
  <div className='application'>
    <Toolbar />
    <GamesGridContainer />
  </div>
);

// #endregion Component