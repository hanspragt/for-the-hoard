// #region Imports

/* React */
import * as React from 'react';
import * as ReactDOM from 'react-dom';

/* Redux */
import {Provider} from 'react-redux';
import Store from './redux/store';

/* Hoard */
import {Application} from './components/application.component';

// #endregion Imports

ReactDOM.render(
  <Provider store={Store}>
    <Application />
  </Provider>,
  document.getElementById('app_mounting_point')
);