// #region Imports

/* Node */
const path = require('path');

/* Webpack */
const htmlPlugin = require('html-webpack-plugin');
const template = require('html-webpack-template');

// #endregion Imports

// #region Constants

const PROJECT_ROOT = path.resolve(path.join(__dirname, '../../'));

// #endregion Constants

// #region Webpack Configuration

module.exports = (env) => {

  const production = env ? env.production : false;

  return {

    /* What we are building. */

    entry: path.join(PROJECT_ROOT, './source/client/typescript/main.tsx'),
    resolve: {
      extensions:     ['.ts', '.tsx', '.js', '.html', '.scss']
    },

    /* How we are building it. */

    mode: production ? 'production' : 'development',
    devtool: production ? 'none' : 'source-map',
    watch: !production,

    module: {
      rules: [

        /* Typescript Files */
        {
          test: /\.ts(x?)$/,
          use: [
            { 
              loader: 'ts-loader',
              options: {
                configFile: path.join(PROJECT_ROOT, './source/client/tsconfig.json')
              }
            },
            {
              loader: 'tslint-loader',
              options: {
                configFile: path.join(PROJECT_ROOT, './source/client/tslint.json')
              }
            }
          ]
        },

        /* Plain Stylesheets */
        {
          test: /\.css$/,
          use: [
            { loader: 'css-loader' }
          ]
        },

        /* Sass Styles */
        {
          test: /\.scss$/,
          use: [
            { loader: 'style-loader' },
            { loader: 'css-loader' },
            { loader: 'sass-loader' }
          ]
        },

        /* Font Files */
        {
          test: /\.(woff(2)?|ttf|eot|svg)$/,
          use: [
            { 
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                outputPath: 'fonts/'
              }
            }
          ]
        }

      ]
    },

    plugins: [
      new htmlPlugin({
        title: 'For the Hoard',
        inject: false,
        template: template,
        appMountId: 'app_mounting_point'
      })
    ],

    /* Where the build output goes. */

    output: {
      path: path.join(PROJECT_ROOT, './distribution', './client'), 
      filename: 'client.bundle.js'
    },

    /* How to serve it while in development. */

    devServer: {
      contentBase: './distribution/client'
    }
  }

}

// #endregion Webpack Configuration